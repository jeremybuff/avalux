<?php if(!class_exists('raintpl')){exit;}?><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>

    This email is being sent to confirm that you have made a payment online.
    <br>
    <br>
    <strong>Invoice:</strong> #<?php echo $number;?><br>
    <strong>Payment amount:</strong> <?php echo get_config('currency_symbol'); ?><?php echo $amount;?>
    <br>
    <br>

<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("footer") . ( substr("footer",-1,1) != "/" ? "/" : "" ) . basename("footer") );?>