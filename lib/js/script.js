$(document).ready(function($){

	$('.tt').tooltip();

	if( $('textarea').length > 0){
		$('textarea').elastic();
		$('textarea').trigger('update');
	}

	$viewportHeight = $(window).height();
	$homeMasthead = $('body.home #masthead');
	$pageMasthead = $('body.page #masthead');

	/* $viewportHeight = ($(window).height() - ($('#introduction').height() + 100)); */

	$homeMasthead.css('height', $viewportHeight);
	$pageMasthead.css('height', ($viewportHeight/2));


	setTimeout(function(){
		$mastheadHeight = (($viewportHeight/2) / 4.5)
	$mastheadImageHeight = ($('#masthead img').height() / 2);
	$mastheadImgTop = ($mastheadHeight - $mastheadImageHeight);
	$('#masthead img').css('margin-top', $mastheadImgTop);
	}, 100)

	$(window).on('resize', function(){
		$viewportHeight = $(window).height();
		if( $('body.home').length > 0){
			$homeMasthead.css('height', $viewportHeight);
		}
		if( $('body.page').length > 0){
			$pageMasthead.css('height', ($viewportHeight/2));

			$mastheadHeight = (($viewportHeight/2) / 4)
			$mastheadImageHeight = ($('#masthead img').height() / 2);
			$mastheadImgTop = $mastheadHeight - $mastheadImageHeight;
			$('#masthead img').css('margin-top', $mastheadImgTop);
		}
	});


	$('#main-nav').affix({
	    offset: {
	      top: $('#masthead').height()
	    }
	})

	$(function($){
	     $( '.menu-btn' ).click(function(){
	     $('.responsive-menu').toggleClass('expand')
	     })
    })

	var colors = new Array(
	  [83,181,222],
	  [43,130,166],
	  [53,49,44],
	  [17,42,53],
	  [138,114,90]);

	var step = 0,
	    colorIndices = [0,1,2,3],
	    gradientSpeed = 0.01;

	function updateGradient(){
		var c0_0 = colors[colorIndices[0]];
		var c0_1 = colors[colorIndices[1]];
		var c1_0 = colors[colorIndices[2]];
		var c1_1 = colors[colorIndices[3]];

		var istep = 1 - step;
		var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
		var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
		var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
		var color1 = "#"+((r1 << 16) | (g1 << 8) | b1).toString(16);

		var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
		var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
		var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
		var color2 = "#"+((r2 << 16) | (g2 << 8) | b2).toString(16);

		$('body.home #masthead').css({
		 	'background': ''+color1+'',
		 	'background': '-moz-linear-gradient(top, '+color1+' 0%, '+color2+' 100%)',
			'background': '-webkit-gradient(linear, left top, left bottom, color-stop(0%,'+color1+'), color-stop(100%,'+color2+'))',
			'background': '-webkit-linear-gradient(top, '+color1+' 0%, '+color2+' 100%)',
			'background': '-o-linear-gradient(top, '+color1+' 0%, '+color2+' 100%)',
			'background': '-ms-linear-gradient(top,  '+color1+' 0%, '+color2+' 100%)',
			'background': 'linear-gradient(to bottom,  '+color1+' 0%, '+color2+' 100%)',
			'filter': 'progid:DXImageTransform.Microsoft.gradient( startColorstr='+color1+', endColorstr='+color2+',GradientType=0 )'});

		  step += gradientSpeed;
		  if ( step >= 1 ) {
		    step %= 1;
		    colorIndices[0] = colorIndices[1];
		    colorIndices[2] = colorIndices[3];

		    //pick two new target color indices
		    //do not pick the same as the current one
		    colorIndices[1] = ( colorIndices[1] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
		    colorIndices[3] = ( colorIndices[3] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
		  }
		}

		setInterval(updateGradient,100);




	/*==========================================================*/
	/* AJAX Contact form
	/*==========================================================*/

	$('#contact-form').submit(function() {
		$('.form-result p').html('<i class="fa fa-cog fa-spin"></i> Sending...');
		$('.form-result').slideDown();


		$.ajax({
			crossDomain: true,
			dataType: 'jsonp',
		    url: 'https://avaluxinc.com/lib/php/send.php',
		    data: $(this).serialize(),
		    jsonp: 'callback',
		    success: function(data) {
		    	if(data.success) {
			    	$('.form-result p').html('<i class="fa fa-check"></i> Thank you. Your message has been sent.');
					$('.form-result').removeClass('fail success').addClass('success').slideDown().delay(5000).slideUp();
		    	} else {
			    	$('.form-result p').html('<i class="fa fa-times"></i> We&#39;re sorry, there was an error sending your message. Please call us at 1-844-AVALUX-1 to speak.');
					$('.form-result').removeClass('fail success').addClass('fail').slideDown();
		    	}


		    },
		    error: function() {
		    	$('.form-result p').html('<i class="fa fa-times"></i> We&#39;re sorry, there was an error sending your message. Please call us at 1-844-AVALUX-1 to speak.');
				$('.form-result').removeClass('fail success').addClass('fail').slideDown();
		    }
		});

		return false;
	});


});
